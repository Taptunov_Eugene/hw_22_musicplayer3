package com.deveducation.model;

import java.util.Objects;

public class Songs {
    private Integer id;
    private String name;
    private String genre;
    private String singer;
    private String link;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String time;
    private String album;
    private String year;

    public Songs(String name, String genre, String singer, String link, String time, String album, String year) {
        this.name = name;
        this.genre = genre;
        this.singer = singer;
        this.link = link;
        this.time = time;
        this.album = album;
        this.year = year;
    }

    public Songs(Integer id, String name, String genre, String singer, String link, String time, String album, String year) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.singer = singer;
        this.link = link;
        this.time = time;
        this.album = album;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Songs songs = (Songs) o;
        return Objects.equals(name, songs.name) &&
                Objects.equals(genre, songs.genre) &&
                Objects.equals(singer, songs.singer) &&
                Objects.equals(link, songs.link) &&
                Objects.equals(time, songs.time) &&
                Objects.equals(album, songs.album) &&
                Objects.equals(year, songs.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, genre, singer, link, time, album, year);
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", singer='" + singer + '\'' +
                ", link='" + link + '\'' +
                ", time='" + time + '\'' +
                ", album='" + album + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
