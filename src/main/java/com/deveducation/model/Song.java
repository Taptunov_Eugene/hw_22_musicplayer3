package com.deveducation.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Song {
    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "id_genre")
    private Genre genre;

    @ManyToOne
    @JoinColumn(name = "id_singer")
    private Singer singer;

    @ManyToOne
    @JoinColumn(name = "id_album")
    private Album album;

    private String link;

    private String time;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return Objects.equals(name, song.name) &&
                Objects.equals(genre, song.genre) &&
                Objects.equals(singer, song.singer) &&
                Objects.equals(album, song.album) &&
                Objects.equals(link, song.link) &&
                Objects.equals(time, song.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, genre, singer, album, link, time);
    }

    public Song() {
    }

    public Song(String name, Genre genre, Singer singer, Album album, String link, String time) {
        this.name = name;
        this.genre = genre;
        this.singer = singer;
        this.album = album;
        this.link = link;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Singer getSinger() {
        return singer;
    }

    public void setSinger(Singer singer) {
        this.singer = singer;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer("Song{");
        buffer.append("id=").append(id);
        buffer.append(", name='").append(name).append('\'');
        buffer.append(", genre=").append(genre);
        buffer.append(", singer=").append(singer);
        buffer.append(", album=").append(album);
        buffer.append(", link='").append(link).append('\'');
        buffer.append(", time='").append(time).append('\'');
        buffer.append('}');
        return buffer.toString();
    }
}
