package com.deveducation.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Singer")
public class Singer {

    @Id
    @GeneratedValue
    private Integer id;

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer("Singer{");
        buffer.append("id=").append(id);
        buffer.append(", genre=").append(genre);
        buffer.append(", name='").append(name).append('\'');
        buffer.append(", albums=").append(albums);
        buffer.append(", songs=").append(songs);
        buffer.append('}');
        return buffer.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Singer artist = (Singer) o;
        return Objects.equals(name, artist.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public List<Genre> getGenre() {
        return genre;
    }

    public void setGenre(List<Genre> genre) {
        this.genre = genre;
    }

    @OneToMany
    private List<Genre> genre = new ArrayList<>();

    private String name;
    @OneToMany
    private final List<Album> albums = new ArrayList<>();

    @OneToMany
    private final List<Song> songs = new ArrayList<>();


    public Singer() {
    }

    public Singer(String name, Genre genre) {
        this.name = name;
        this.genre.add(genre);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSingers() {
        return name;
    }

    public void setSingers(String singers) {
        this.name = singers;
    }


}
