package com.deveducation.servlet;

import com.deveducation.config.Configure;
import com.deveducation.model.Genre;
import com.deveducation.model.Singer;
import com.google.gson.Gson;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "SingersServlet", urlPatterns = "/getSingers")
public class SingerServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String genre = request.getParameter("genre");

        EntityManagerFactory entityManagerFactory = Configure.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        entityManager2.getTransaction().begin();
        List<Singer> result2 = entityManager2.createQuery("SELECT a FROM Singer a ", Singer.class).getResultList();
        Set<Singer> artistList = new HashSet<>();
        entityManager2.getTransaction().commit();
        System.out.println(result2);

        for (Singer a : result2) {
            List<Genre> genres = a.getGenre();
            for (Genre genres2 : genres) {
                if (genres2.getGenre().equals(genre)) {
                    artistList.add(a);
                }
            }
        }
        System.out.println(result2);
        StringBuilder stringList = new StringBuilder();
        for (Singer artist : artistList) {
            stringList.append(artist.getName()).append(",");
        }
        out.print(new Gson().toJson(stringList.toString()));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}


