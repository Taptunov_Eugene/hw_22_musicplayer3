package com.deveducation.servlet;

import com.deveducation.config.Configure;
import com.deveducation.model.*;
import com.google.gson.Gson;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@WebServlet(name = "ArtistsSong", urlPatterns = "/addSong")
public class GenreServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-16");

        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        EntityManagerFactory entityManagerFactory = Configure.getEntityManager();
        String url = request.getParameter("url");
        System.out.println(url);
        File file = new File(url);
        String[] musics = file.list();

        try {
            int i;
            StringBuilder stringList = new StringBuilder();
            Set<Genre> result;
            EntityManager entityManager1 = entityManagerFactory.createEntityManager();
            EntityManager entityManager5 = entityManagerFactory.createEntityManager();

            entityManager5.getTransaction().begin();
            List<Song> result4 = entityManager5.createQuery("SELECT a FROM Song a ", Song.class).getResultList();

            Set<Songs> songs = new HashSet<>();
            for (Song a : result4) {
                songs.add(new Songs(a.getName(), a.getGenre().getGenre(), a.getSinger().getName(), a.getLink(),
                        a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
            }
            for (i = 0; i < Objects.requireNonNull(musics).length; i++) {
                boolean isExist = false;

                EntityManager entityManager2 = entityManagerFactory.createEntityManager();
                EntityManager entityManager3 = entityManagerFactory.createEntityManager();
                EntityManager entityManager4 = entityManagerFactory.createEntityManager();

                Mp3File mp3File = new Mp3File(url + "/" + musics[i]);
                String name = musics[i];
                String singer1 = mp3File.getId3v2Tag().getArtist();
                String link = url + "/" + musics[i];
                String genre = mp3File.getId3v2Tag().getGenreDescription();
                String album = mp3File.getId3v2Tag().getAlbum();
                String time = String.valueOf(mp3File.getId3v2Tag().getLength());
                String year = mp3File.getId3v2Tag().getYear();

                for (Songs song : songs) {
                    if (song.getLink().equals(link)) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    Genre genre1 = new Genre(genre);
                    entityManager2.getTransaction().begin();
                    entityManager2.persist(genre1);
                    entityManager2.getTransaction().commit();

                    Singer singer = new Singer(singer1, genre1);
                    entityManager2.getTransaction().begin();
                    entityManager2.persist(singer);
                    entityManager2.getTransaction().commit();

                    Album album1 = new Album(singer, album, year);
                    entityManager3.getTransaction().begin();
                    entityManager3.persist(album1);
                    entityManager3.getTransaction().commit();

                    Song song1 = new Song(name, genre1, singer, album1, link, time);
                    entityManager4.getTransaction().begin();
                    entityManager4.persist(song1);
                    entityManager4.getTransaction().commit();
                }
            }

            entityManager1.getTransaction().begin();
            result = new HashSet<>(entityManager1.createQuery("from Genre", Genre.class).getResultList());
            entityManager1.getTransaction().commit();
            System.out.println(result);
            for (Genre genre : result) {
                stringList.append(genre.getGenre()).append(",");
            }
            out.print(new Gson().toJson(stringList.toString()));
        } catch (UnsupportedTagException | InvalidDataException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}


