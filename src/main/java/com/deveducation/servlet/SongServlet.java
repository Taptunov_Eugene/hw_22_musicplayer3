package com.deveducation.servlet;

import com.deveducation.config.Configure;
import com.deveducation.model.Song;
import com.deveducation.model.Songs;
import com.google.gson.Gson;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "SingersServlet2", urlPatterns = "/getSong")
public class SongServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String genre = request.getParameter("genre");
        String singer = request.getParameter("singer");

        EntityManagerFactory entityManagerFactory = Configure.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");


        entityManager2.getTransaction().begin();
        List<Song> result4 = entityManager2.createQuery("SELECT a FROM Song a ", Song.class).getResultList();
        entityManager2.getTransaction().commit();
        System.out.println(result4);

        Set<Songs> songs = new HashSet<>();
        for (Song a : result4) {
            if (a.getGenre().getGenre().equals(genre) && a.getSinger().getName().equals(singer)) {
                songs.add(new Songs(a.getName(), a.getGenre().getGenre(), a.getSinger().getName(), a.getLink(),
                        a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
            }
        }
        out.print(new Gson().toJson(songs));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }
}


