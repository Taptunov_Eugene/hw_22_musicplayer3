package com.deveducation.application;

import com.deveducation.model.Genre;
import com.deveducation.model.Singer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("train");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();

        Genre genre1 = new Genre("genre");
        entityManager.getTransaction().begin();
        entityManager.persist(genre1);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        List<Genre> result = entityManager.createQuery("from Genre", Genre.class).getResultList();
        entityManager.getTransaction().commit();
        System.out.println(result);

        Singer singer = new Singer("singer", genre1);
        entityManager2.getTransaction().begin();
        entityManager2.persist(singer);
        entityManager2.getTransaction().commit();

        List<Singer> singerList = new ArrayList<>();

        entityManager2.getTransaction().begin();
        List<Singer> result2 = entityManager2.createQuery("SELECT a FROM Singer a ", Singer.class).getResultList();
        entityManager2.getTransaction().commit();
        for (Singer a : result2) {
            List<Genre> genres = a.getGenre();
            for (Genre genres2 : genres) {
                if (genres2.getGenre().equals("genre")) {
                    singerList.add(a);
                    System.out.println("asd");
                }
            }
        }
        System.out.println(result2);
    }
}
