package com.deveducation.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Database {
    private static final String URL = "jdbc:mysql://q2gen47hi68k1yrb.chr7pe7iynqr.eu-west-1.rds.amazonaws.com:3306/j9ckb5nt63456xj6?useUnicode=true&serverTimezone=UTC";
    private static final String USER = "acabwiiupk2vgu9q";
    private static final String PASSWORD = "muggpyqupd3hgcme";
    private static final Logger logger = LoggerFactory.getLogger(Database.class);
    private Statement statement;
    private Connection connection;

    public Database() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            if (connection != null) {
                logger.info("Connection was successful");
            }
        } catch (ClassNotFoundException e) {
            logger.error("Where is your mySQL JDBC Driver?");
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("Failed to make connection!");
            e.printStackTrace();
        }
        createTable();
        clearTable();
    }

    private void createTable() {
        try {
            statement = connection.createStatement();
            String query = "CREATE TABLE IF NOT EXISTS songs(number INT NOT NULL);";
            statement.executeUpdate(query);
            logger.info("Table was created");
        } catch (SQLException e) {
            logger.error("Something went wrong");
            e.printStackTrace();
        }
    }

    private void clearTable() {
        try {
            statement = connection.createStatement();
            String query = "TRUNCATE TABLE songs";
            statement.executeUpdate(query);
            logger.info("Table was created");
        } catch (SQLException e) {
            logger.error("Something went wrong");
            e.printStackTrace();
        }
    }

    public String getSongs() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            statement = connection.createStatement();
            String query = "SELECT * FROM songs";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                stringBuilder.append(resultSet.getString(1)).append(",");
            }
            logger.info("Data was selected");
        } catch (SQLException e) {
            logger.error("Something went wrong");
            e.printStackTrace();
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        return stringBuilder.toString();
    }
}
