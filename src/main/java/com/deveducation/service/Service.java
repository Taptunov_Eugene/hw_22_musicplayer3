package com.deveducation.service;

import com.deveducation.config.Configure;
import com.deveducation.model.Singer;
import com.deveducation.model.Genre;
import com.deveducation.model.Song;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class Service {
    private static final EntityManager entityManager;

    static {
        entityManager = Configure.getEntityManager().createEntityManager();
    }

    public static List<Genre> getAllGenres() {
        entityManager.getTransaction().begin();
        List<Genre> result = entityManager.createQuery("from Genre", Genre.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    public static List<Singer> getAllSingerByGenre(Genre genre) {
        TypedQuery<Singer> query =
                entityManager.createQuery("SELECT a FROM Singer a where a.genre=" + genre.getGenre(), Singer.class);
        return query.getResultList();
    }

    public static List<Song> getAllSongsByGenreAndSinger(Genre genre, Singer singer) {
        TypedQuery<Song> query = entityManager.createQuery("SELECT s FROM Song s where s.genre=" + genre.getGenre()
                        + " and s.singer=" + singer.getName(), Song.class);
        return query.getResultList();
    }

}
