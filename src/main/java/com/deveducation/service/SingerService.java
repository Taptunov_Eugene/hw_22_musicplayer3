package com.deveducation.service;

import javax.persistence.EntityManager;

import com.deveducation.config.Configure;
import com.deveducation.model.Singer;

public class SingerService {
    private static final EntityManager entityManager;

    static {
        entityManager = Configure.getEntityManager().createEntityManager();
    }

    public static void save(Singer singer) {
        entityManager.getTransaction().begin();
        entityManager.persist(singer);
        entityManager.getTransaction().commit();
    }
}
