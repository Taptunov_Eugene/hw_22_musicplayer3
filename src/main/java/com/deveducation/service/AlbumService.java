package com.deveducation.service;

import com.deveducation.model.Album;
import com.deveducation.config.Configure;

import javax.persistence.EntityManager;

public class AlbumService {
    private static final EntityManager entityManager;

    static {
        entityManager = Configure.getEntityManager().createEntityManager();
    }

    public static void save(Album album) {
        entityManager.getTransaction().begin();
        entityManager.persist(album);
        entityManager.getTransaction().commit();
    }
}
