package com.deveducation.service;

import com.deveducation.config.Configure;
import com.deveducation.model.Song;

import javax.persistence.EntityManager;

public class SongService {

    private static final EntityManager entityManager;

    static {
        entityManager = Configure.getEntityManager().createEntityManager();
    }

    public static void save(Song song) {
        entityManager.getTransaction().begin();
        entityManager.persist(song);
        entityManager.getTransaction().commit();
    }
}
