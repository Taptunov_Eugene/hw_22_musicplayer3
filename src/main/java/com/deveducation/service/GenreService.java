package com.deveducation.service;

import com.deveducation.config.Configure;
import com.deveducation.model.Genre;

import javax.persistence.EntityManager;

public class GenreService {
    private final EntityManager entityManager;

    public GenreService() throws ClassNotFoundException {
        entityManager = Configure.getEntityManager().createEntityManager();
    }

    public void save(Genre genre) {
        entityManager.getTransaction().begin();
        entityManager.persist(genre);
        entityManager.getTransaction().commit();
    }
}
