package com.deveducation.crud;

import com.deveducation.config.Configure;
import com.deveducation.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "SingersServlet7", urlPatterns = "/create")
public class Create extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        EntityManagerFactory entityManagerFactory = Configure.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();
        EntityManager entityManager3 = entityManagerFactory.createEntityManager();
        EntityManager entityManager4 = entityManagerFactory.createEntityManager();
        EntityManager entityManager5 = entityManagerFactory.createEntityManager();
        entityManager5.getTransaction().begin();
        List<Song> result5 = entityManager5.createQuery("SELECT a FROM Song a ", Song.class).getResultList();

        Set<Songs> songs = new HashSet<>();
        for (Song a : result5) {
            songs.add(new Songs(a.getName(), a.getGenre().getGenre(), a.getSinger().getName(), a.getLink(),
                    a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
        }
        boolean isExist = false;

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String name = req.getParameter("name");
        String genre12 = req.getParameter("genre");
        String singer13 = req.getParameter("singer");
        String album14 = req.getParameter("album");
        String link = req.getParameter("link");
        String time = req.getParameter("time");
        String year = req.getParameter("year");

        for (Songs tracks : songs) {
            if (tracks.getLink().equals(link)) {
                isExist = true;
                break;
            }
        }
        if (!isExist) {
            Genre genre1 = new Genre(genre12);
            entityManager2.getTransaction().begin();
            entityManager2.persist(genre1);
            entityManager2.getTransaction().commit();

            Singer singer1 = new Singer(singer13, genre1);
            entityManager2.getTransaction().begin();
            entityManager2.persist(singer1);
            entityManager2.getTransaction().commit();

            Album album1 = new Album(singer1, album14, year);
            entityManager3.getTransaction().begin();
            entityManager3.persist(album1);
            entityManager3.getTransaction().commit();

            Song song1 = new Song(name, genre1, singer1, album1, link, time);
            entityManager4.getTransaction().begin();
            entityManager4.persist(song1);
            entityManager4.getTransaction().commit();
        }
        entityManager2.getTransaction().begin();
        List<Song> result4 = entityManager2.createQuery("SELECT a FROM Song a ", Song.class).getResultList();

        Set<Songs> tracks2 = new HashSet<>();
        for (Song a : result4) {
            tracks2.add(new Songs(a.getId(), a.getName(), a.getGenre().getGenre(), a.getSinger().getName(),
                    a.getLink(), a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
        }
        req.setAttribute("songs", tracks2);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

}


