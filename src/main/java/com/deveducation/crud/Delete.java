package com.deveducation.crud;

import com.deveducation.config.Configure;
import com.deveducation.model.Song;
import com.deveducation.model.Songs;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "SingersServlet10", urlPatterns = "/delete")
public class Delete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EntityManagerFactory entityManagerFactory = Configure.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();

        String id = req.getParameter("id");
        entityManager2.getTransaction().begin();
        Song song = entityManager2.createQuery("SELECT a FROM Song a where a.id =" + id, Song.class).getSingleResult();

        entityManager2.remove(song);
        entityManager2.getTransaction().commit();
        entityManager2.getTransaction().begin();

        List<Song> result4 = entityManager2.createQuery("SELECT a FROM Song a ", Song.class).getResultList();
        Set<Songs> songs = new HashSet<>();
        for (Song a : result4) {
            songs.add(new Songs(a.getId(), a.getName(), a.getGenre().getGenre(), a.getSinger().getName(),
                    a.getLink(), a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
        }
        entityManager2.getTransaction().commit();
        req.setAttribute("songs", songs);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

}


