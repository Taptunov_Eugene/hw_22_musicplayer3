package com.deveducation.crud;

import com.deveducation.config.Configure;
import com.deveducation.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@WebServlet(name = "SingersServlet9", urlPatterns = "/update")
public class Update extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        EntityManagerFactory entityManagerFactory = Configure.getEntityManager();
        EntityManager entityManager2 = entityManagerFactory.createEntityManager();

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String genre12 = req.getParameter("genre");
        String singer13 = req.getParameter("singer");
        String album14 = req.getParameter("album");
        String link = req.getParameter("link");
        String time = req.getParameter("time");
        String year = req.getParameter("year");

        entityManager2.getTransaction().begin();
        Song song = entityManager2.createQuery("SELECT a FROM Song a where a.id =" + id, Song.class).getSingleResult();
        song.setName(name);
        song.getGenre().setGenre(genre12);
        song.getSinger().setSingers(singer13);
        song.getAlbum().setAlbum(album14);
        song.setLink(link);
        song.setTime(time);
        song.getAlbum().setYear(year);

        entityManager2.getTransaction().commit();
        entityManager2.getTransaction().begin();
        List<Song> result4 = entityManager2.createQuery("SELECT a FROM Song a ", Song.class).getResultList();

        Set<Songs> songs = new HashSet<>();
        for (Song a : result4) {
            songs.add(new Songs(a.getId(), a.getName(), a.getGenre().getGenre(), a.getSinger().getName(), a.getLink(),
                    a.getTime(), a.getAlbum().getAlbum(), a.getAlbum().getYear()));
        }
        entityManager2.getTransaction().commit();
        req.setAttribute("songs", songs);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("start.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

}


