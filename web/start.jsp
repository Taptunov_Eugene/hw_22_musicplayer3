<%--
  Created by IntelliJ IDEA.
  User: Jack
  Date: 19.10.2020
  Time: 0:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.deveducation.model.Song" %>
<%@ page import="com.deveducation.model.Songs" %>
<%@ page import="java.util.HashSet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main</title>
</head>
<body>

<form action="read">
    <input type="submit" value="read">
</form>

<form action="create">
    <input type="text" name="name">
    <input type="text" name="genre">
    <input type="text" name="artist">
    <input type="text" name="album">
    <input type="text" name="link">
    <input type="text" name="time">
    <input type="year" name="year">
    </br>
    <input type="submit" value="create">
</form>

<form action="update">
    <input type="text" name="id">
    <input type="text" name="name">
    <input type="text" name="genre">
    <input type="text" name="artist">
    <input type="text" name="album">
    <input type="text" name="link">
    <input type="text" name="time">
    <input type="year" name="year">
    </br>
    <input type="submit" value="update">
</form>

<form action="delete">

    <input type="text" name="id">
    </br>
    <input type="submit" value="delete">
</form>
</br>
<table>
    <%
        HashSet<Songs> names = (HashSet<Songs>) request.getAttribute("songs");
        if (names != null && !names.isEmpty()) {
            for (Songs songs : names) {
                out.println("<tr><td>" + songs.getId() + "</td><td>" + songs.getName() + "</td><td>"
                        + songs.getGenre() + "</td><td>" + songs.getSinger() + "</td><td>" + songs.getAlbum() + "</td><td>"
                        + songs.getLink() + "</td><td>" + songs.getTime() + "</td></tr>");
            }
        }
    %>
</table>
</body>
</html>
